<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/otra', function (Request $request) {
		
	$user = Auth::user();
	if($user->SuperAdmin()){
		echo "Eres Super Admin";
	}else{
		echo "No eres Super Admin";
	}
});

Route::group(['prefix'=>'v1'], function(){

	Route::resource('rol', 'RoleControllerApi');
	Route::resource('user', 'UserControllerApi');

});
