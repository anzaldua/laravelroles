<?php

namespace App\Listeners;

use App\Events\Modificacion;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\CorreoModificacion;
use Illuminate\Support\Facades\Mail;

class ModificacionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Modificacion  $event
     * @return void
     */
    public function handle(Modificacion $event)
    {
        Mail::to($event->cambio['adminEmail'])->send( new CorreoModificacion( $event->cambio ) );
    }
}
