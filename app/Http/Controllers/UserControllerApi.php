<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Events\Modificacion;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;

class UserControllerApi extends Controller
{
    public function __construct()
    {

        $user = $this->middleware('auth:api');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
        $user = User::all();
        $response = Response::json($user, 200);

        $role = Auth::user();

        if($role->Autorizado()){

            return $response;

        }else{

            return "Permiso no autorizado";

        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = new User();

        $role = Auth::user();

        if($role->Autorizado()){

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = $request->input('password');

            $user->save();

            $name = $user->name;
            $email = $user->email;
            $adminEmail = User::where('role_id', 1)->get("email");

            $cambio = array('name'=>$name, 'email'=>$email, 'adminEmail'=>$adminEmail);

            event( new Modificacion($cambio) );

            return response()->json($user);

        }else{

            return "Permiso no autorizado";

        }

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
