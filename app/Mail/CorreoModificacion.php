<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CorreoModificacion extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $cambio;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $cambio )
    {
        $this->cambio = $cambio;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('kinedu@es.com')
                    ->view('plantillaCorreo')
                    ->with('cambio', $this->cambio);
    }
}
